<?php

include 'coffeescript.admin.inc';

/**
 * Implements hook_preprocessor_info().
 */
function coffeescript_preprocessor_info() {
  return array(
    'coffeescript' => array(
      'label' => 'CoffeeScript PHP',
      'callback' => 'parse_callback',
      'filetypes' => array('coffee'),
    )
  );
}


/**
 * Parse a CoffeeScript string and transform it into JS.
 * Callback from prepro module.
 *
 * @return
 *   The JS as a string.
 */
function coffeescript_parse_callback($file, $local, $global) {
  $local += array(
    'errors' => 'watchdog',
  );
  extract ($local);

  try {
    return coffeescript_parse($file['contents']);
  }
  catch (Exception $e) {
    $message = 'CoffeeScript PHP error: "@message" (%filename).';
    $variables = array('%filename' => $file['data'], '@message' => str_replace(array('"', "\n"), array("'", '\A'), $e->getMessage()));
    if ($errors == 'watchdog') {
      watchdog('coffeescript', $message, $variables, WATCHDOG_ERROR);
      if (user_access('administer site configuration')) {
        drupal_set_message(t('An error occured while processing !filename. Please consult your !watchdog for a detailed error description.', array('!filename' => l(basename($file['data']), $file['data']), '!watchdog' => l('log messages', 'admin/reports/dblog'))), 'error');
      }
    }
    else if ($errors == 'output') {
      return theme_render_template(drupal_get_path('module', 'coffeescript') . '/coffeescript_error.js', array('error' => t($message, $variables)));
    }
  }
}

/**
 * Convenience function for compiling a string of coffeescript into a string
 * of javascript.
 *
 * @return Complied Javascript or NULL if library could not be found.
 */
function coffeescript_parse($coffeescript_string) {
  if (_coffeescript_load_library()) {
    return CoffeeScript\Compiler::compile($coffeescript_string);
  }
}

/**
 * Helper function which ensures we only load/initialize the library once.
 */
function _coffeescript_load_library() {
  $success = drupal_static(__FUNCTION__);
  if (!isset($success)) {
    $success = FALSE;
    $path = libraries_get_path('coffeescript-php');
    $library = $path . '/src/CoffeeScript/Init.php';
    if ($path && file_exists($library)) {
      require_once ($library);
      CoffeeScript\Init::load();
      $success = TRUE;
    }
  }
  return $success;
}
